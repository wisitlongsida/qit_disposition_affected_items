import logging
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from time import sleep
import pandas as pd
import configparser
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import sys
import datetime
import traceback
import chromedriver_autoinstaller
from selenium.webdriver.chrome.service import Service


class QIT_DISPOSITION:

    def __init__(self):

        # create logger
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)

        # create console handler
        ch = logging.StreamHandler()

        #create file handler 
        date = str(datetime.datetime.now().strftime('%d-%b-%Y %H_%M_%S %p'))
        fh = logging.FileHandler('debug\\{}.log'.format(date),encoding='utf-8')

        # create formatter
        formatter = logging.Formatter('%(asctime)s - %(funcName)s - %(lineno)d - %(levelname)s - %(message)s',datefmt='%d/%b/%Y %I:%M:%S %p')

        # add formatter to ch
        ch.setFormatter(formatter)

        #add formatter to fh
        fh.setFormatter(formatter)

        # add ch to logger
        self.logger.addHandler(ch)

        #add fh to logger
        self.logger.addHandler(fh)


        #config.init file
        self.my_config_parser = configparser.ConfigParser()

        self.my_config_parser.read('config\\config.ini')

        self.config = { 

        'email': self.my_config_parser.get('QIT_DISPOSITION_CONFIG','email'),
        'password': self.my_config_parser.get('QIT_DISPOSITION_CONFIG','password'),

        }

        #init chrome driver
        self.driver_path = chromedriver_autoinstaller.install()
        self.logger.debug("Check chromedriver updating >>> "+self.driver_path)

        self.err = {}

        self.open_farc = {}

        self.farc_fa_case = []

        self.can_not_update_state = {}

        self.farc_res = {}


    def login(self):

        self.driver=webdriver.Chrome(service=Service(self.driver_path))

        self.driver.get('https://www-plmprd.cisco.com/Agile/')

        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@id="userInput"]'))).send_keys(self.config["email"])

        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="login-button"]'))).click()

        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@id="passwordInput"]'))).send_keys(self.config["password"])

        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@id="login-button"]'))).click()

        count_render_2fa = 0

        while (self.driver.title != "Universal Prompt"):

            sleep(1)

            count_render_2fa+=1

            self.logger.info("Wait for Universal Prompt render:"+str(count_render_2fa))
            
        
        while True:
            
            try:

                WebDriverWait(self.driver, 60).until(ec.visibility_of_element_located((By.XPATH, '//button[@id="trust-browser-button"]'))).click()
                
                break
                
            except:
                
                self.logger.warning('Not found trust browser button')

        two_fa_url=self.driver.current_url

        count_duo_pass = 0

        while(two_fa_url==self.driver.current_url):

            sleep(1)

            count_duo_pass+=1

            self.logger.info("Wait for count_duo_pass:"+str(count_duo_pass))

            if count_duo_pass == 30:

                self.logger.warning("!!! LOGIN TIMEOUT !!!")

                self.driver.quit()

                sys.exit()

        self.logger.info("Login to QIS is success!!!")

        sleep(1)

        self.driver.get('https://www-plmprd.cisco.com/Agile/')

        self.main_page = self.driver.current_window_handle

        self.logger.debug("Main Page:"+self.main_page)

        handles = self.driver.window_handles

        for handle in handles:

            sleep(1)

            self.driver.switch_to.window(handle)

            if self.main_page != self.driver.current_window_handle:

                self.driver.close()

        self.driver.switch_to.window(self.main_page)

        self.driver.maximize_window()

        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//div[@title="Collapse Left Navigation"]'))).click()

        return True


    def extract_data_excel(self):                                                                                                                                            

        self.fa_dict = {}

        self.df = pd.read_excel("qit_disposition_template.xlsm")
        
        self.logger.info(self.df)

        index = self.df.index

        number_of_rows = len(index)
        
        self.logger.info('Number of rows >>> '+str(number_of_rows))

        for i in range(number_of_rows):

            if self.df['FA#'][i] in self.fa_dict:

                self.fa_dict[self.df['FA#'][i]].update({self.df['Site Received Serial#'][i]:[self.df['QIT Disposition'][i], str(self.df['Problem Description'][i]).replace('_x000D_', ''),self.df['Case Owner'][i],self.df['PID'][i]]})
            
            else:

                self.fa_dict[self.df['FA#'][i]] = {self.df['Site Received Serial#'][i]: [self.df['QIT Disposition'][i], str(self.df['Problem Description'][i]).replace('_x000D_', ''),self.df['Case Owner'][i],self.df['PID'][i]]}

        for i,j in self.fa_dict.items():

            self.logger.debug(i + str(j))

        self.logger.info('FA Case number >>> '+str(self.fa_dict.__len__()) + ' Cases')


    def dispose_affected_items(self,fa_case):

        open_farc = False

        send_for_rc_rp = False

        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(Keys.CONTROL+'a',Keys.BACKSPACE)

        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(fa_case)

        self.logger.info('FA Case >>> '+fa_case)

        while True:

            try:

                WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()

                break
            
            except:

                self.logger.warning("Wait for against click intercepted !!!")

                sleep(1)

        
        #fix bug in case that change priority 
        while True:

            try:
                
                self.fa_status = WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//h2[@style="color:Blue;"]'))).text
                
                self.logger.info('Status of '+fa_case+' >>> '+self.fa_status)
                
                
                #check case status
                if self.fa_status not in ['Fault Duplication','Fault Isolation','Pending Closure Approval']:
                    
                    self.logger.warning('Skip '+fa_case+ ' >>> The status is not accept.')
                    
                    return False
                
                #click on to affected items tab
                WebDriverWait(self.driver, 5).until(ec.visibility_of_all_elements_located((By.XPATH, '//div[@id="tabsDiv"]//li')))[-5].click()

                break

            except:

                self.logger.warning("This case wase changed the priority !!!")

                if fa_case in WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//h4[@id="searchResultHeader"]'))).text:

                    rows_exact = int(WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//strong[@id="totalCount_QUICKSEARCH_TABLE"]'))).text)

                    self.logger.debug(str(rows_exact))

                    rows = WebDriverWait(self.driver, 20).until(ec.visibility_of_all_elements_located((By.XPATH, '//tr[@class="GMDataRow"]')))

                    rows_len = len(rows)

                    self.logger.info("Exact rows >>> "+str(rows_exact)+" Rows number >>> "+str(rows_len))

                    if int(rows_exact)*2 != rows_len:

                        self.logger.critical(fa_case + ': Rows number does not match !!!')

                        self.err.update({fa_case: 'Rows number does not match'})

                    row_start = int(rows_len/2)
                    
                    for i in range(row_start, rows_len):

                        row = rows[i]
                        
                        self.logger.debug(row)

                        entries = row.find_elements(By.TAG_NAME,'td')

                        fa_link = entries[3]

                        self.logger.info("FA Link : " + fa_link.text)

                        if fa_case == fa_link.text.strip():

                            fa_link.click()

                            sleep(1)
                            
                            break
                        
                else:

                    self.logger.critical(fa_case+" : Page is not rendering !!!")
                    
                    WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(Keys.CONTROL+'a',Keys.BACKSPACE)

                    WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(fa_case)

                    WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()             


        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//select[@name="TABLE_VIEWS_LIST_1"]//option[@title="QIT_Disposition"]'))).click()

        sleep(1)

        rows_exact = int(WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//strong[@id="totalCount_PSRTABLE_AFFECTEDITEMS"]'))).text)

        self.logger.debug(str(rows_exact))

        # get html page source

        # self.htmlSource = self.driver.page_source

        # with open("htmlSource.txt",'w',encoding='utf-8') as f:

        #     f.write(self.htmlSource)
        

        #handle to case which has more than 30 units.
        exit_loop_more_than_30_cases = False
        while True:
            
            while True:

                WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//tr[@class="GMHeaderRow"]//span[@title="QIT  Disposition PID/Comp"]'))).click()
                
                sleep(1)
                
                try:
                
                    WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//img[@title="Ascending"]')))
                    
                    self.logger.debug('Check Sort is pass.')
                    
                    break
                
                except:
                    
                    self.logger.warning('Chech Sort is fail.')
        
            sleep(1)

            rows = WebDriverWait(self.driver, 20).until(ec.visibility_of_all_elements_located((By.XPATH, '//tr[@class="GMDataRow"]')))

            rows_len = len(rows)

            self.logger.info("Exact rows >>> "+str(rows_exact)+" Rows number >>> "+str(rows_len))

            if int(rows_exact)*2 != rows_len:

                self.logger.critical(fa_case + ': Rows number does not match !!!')

                self.err.update({fa_case: 'Rows number does not match'})

            row_start = int(rows_len/2)
            
            for i in range(row_start, rows_len):

                row = rows[i]

                self.logger.debug(row)
                
                entries = row.find_elements(By.TAG_NAME,'td')
                
                if int(rows_exact) >= 30:
                    
                    bulkUnit = True
                    
                    dispose = entries[10]
                    
                    if dispose.text.strip() != '':
            
                        WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//span[@id="MSG_Save_1span"]'))).click()
                        
                        self.logger.debug(fa_case+ ' is bulk units that complete.')
                        
                        exit_loop_more_than_30_cases = True
                                
                        break
                    
                else:
                    
                    bulkUnit = False
                    

                fa_flag = entries[2]

                self.logger.info("FA FLAG >>> "+fa_flag.text)

                entries[1].click()

                if fa_flag.text.strip() == 'Yes':

                    sn = entries[4].text.strip()

                    if sn == "":

                        self.err.update({fa_case:' >>> Not found Serial Number'})

                        self.logger.critical(fa_case+' >>> Not found Serial Number')

                        self.press_down()

                        sleep(1)

                        continue

                    self.logger.info('Serial Number >>> '+sn)

                    try:

                        self.logger.info('Dispose to >>> '+self.fa_dict[fa_case][sn][0])

                    except:

                        self.err.update({fa_case:' >>> Not found Disposition >>> '+sn})

                        self.logger.critical(fa_case+' >>> Not found Disposition >>> '+sn)

                        self.press_down()

                        sleep(1)

                        continue

                elif fa_flag.text.strip() == '':

                    sn = entries[5].text.strip()

                    if sn == "":

                        self.err.update({fa_case:' >>> Not found Serial Number'})

                        self.logger.critical(fa_case+' >>> Not found Serial Number')

                        self.press_down()

                        continue

                    self.logger.info('Serial Number >>> '+sn)

                    try:

                        self.logger.info('Dispose to >>> '+self.fa_dict[fa_case][sn][0])

                    except:

                        self.err.update({fa_case:' >>> Not found Disposition >>> '+sn})

                        self.logger.critical(fa_case+' >>> Not found Disposition >>> '+sn)

                        self.press_down()

                        sleep(1)

                        continue

                    dispose = entries[10]

                    if dispose.text.strip() == '':

                        ActionChains(self.driver).double_click(dispose).perform()
                        
                    if 'scrap' in self.fa_dict[fa_case][sn][0].lower():

                        self.press_down(time =4)

                    elif 'root cause' in self.fa_dict[fa_case][sn][0].lower() or 'replacement' in self.fa_dict[fa_case][sn][0].lower() :

                        self.press_down(time =5)   
                        
                    else:

                        self.err.update({fa_case:' >>> Invalid Disposition for SN component >>> '+sn})

                        self.logger.critical(fa_case+' >>> Invalid Disposition for SN component >>> '+sn)
                        

                    self.press_enter()

                    sleep(1)

                    if 'root cause' not in self.fa_dict[fa_case][sn][0].lower() or '40/100' in self.fa_dict[fa_case][sn][3].lower():

                        self.press_down()

                        continue

                    oem = entries[11]

                    if oem.text.strip() == '':

                        ActionChains(self.driver).double_click(oem).perform()

                        self.press_down(time =2)

                        self.press_enter()

                        sleep(1)

                    if 'normal' in self.fa_dict[fa_case][sn][0].lower() or 'urgent' in self.fa_dict[fa_case][sn][0].lower():

                        rc_priority = entries[9]

                        
                        if rc_priority.text.strip() == '':

                            ActionChains(self.driver).double_click(rc_priority).perform()

                            if 'normal' in self.fa_dict[fa_case][sn][0].lower():

                                self.press_down()


                            elif 'urgent' in self.fa_dict[fa_case][sn][0].lower():

                                self.press_down(time =2)

                            else:

                                raise Exception("Error send for rc status")

                            self.press_enter()

                            sleep(1)

                        create_oem = entries[8]

                        if create_oem.text.strip() == '':

                            ActionChains(self.driver).double_click(create_oem).perform()

                            self.press_down(time =2)

                            self.press_enter()

                            sleep(1)

                        rc_path = entries[7]

                        if rc_path.text.strip() == '':

                            ActionChains(self.driver).double_click(rc_path).perform()

                            self.press_down(5)

                            self.press_enter()

                            sleep(1)

                        reason = entries[6]

                        if reason.text.strip() == '':

                            ActionChains(self.driver).double_click(reason).perform()

                            self.press_down(time =4)

                            self.press_enter()

                            sleep(1)

                        site_received = entries[4]

                        
                        if site_received.text.strip() == '':

                            ActionChains(self.driver).double_click(site_received).perform()

                            ActionChains(self.driver).send_keys(sn).perform()

                            self.press_enter()

                            sleep(1)

                        inventory = entries[3]

                        if inventory.text.strip() == '':

                            ActionChains(self.driver).double_click(inventory).perform()

                            ActionChains(self.driver).send_keys("TH5").perform()

                            self.press_enter()

                            sleep(1)

                        self.press_down()

                        sleep(1)

                        continue

                else:

                    self.press_down()

                    continue

                dispose = entries[10]

                if dispose.text.strip() == '':

                    ActionChains(self.driver).double_click(dispose).perform()

                    if 'dgi' in self.fa_dict[fa_case][sn][0].lower():

                        self.press_down()

                    elif 'hold' in self.fa_dict[fa_case][sn][0].lower():

                        self.press_down(time =2)

                    elif 'scrap' in self.fa_dict[fa_case][sn][0].lower():

                        self.press_down(time =4)

                    elif 'replacement' in self.fa_dict[fa_case][sn][0].lower() :

                        self.press_down(time =5)   

                        #17-Aug-2022 new implement cause all cases that in "PCA" have already updated SRE_Symptom
                        #send_for_rc_rp = True

                    elif 'root cause' in self.fa_dict[fa_case][sn][0].lower() :

                        self.press_down(time =5)

                        send_for_rc_rp = True

                        if '40/100' not in self.fa_dict[fa_case][sn][3].lower() :

                            open_farc = True

                self.press_enter()

                sleep(1)

                self.press_down()

                sleep(1)
                
                #finish a rows
                
            
            sleep(1)
            WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//span[@id="MSG_Save_1span"]'))).click()
            
            if exit_loop_more_than_30_cases:
                
                break
            
            #repeat for bulk units
            if bulkUnit:
                
                while True:

                    try:

                        WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()

                        break
                    
                    except:

                        self.logger.warning("Wait for against click intercepted !!!")
                
                #fix bug in case that change priority 
                while True:

                    try:
    
                        #click on to affected items tab
                        WebDriverWait(self.driver, 20).until(ec.visibility_of_all_elements_located((By.XPATH, '//div[@id="tabsDiv"]//li')))[-5].click()

                        break

                    except:

                        self.logger.warning("This case wase changed the priority !!!")

                        if fa_case in WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//h4[@id="searchResultHeader"]'))).text:

                            rows_exact = int(WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//strong[@id="totalCount_QUICKSEARCH_TABLE"]'))).text)

                            self.logger.debug(str(rows_exact))

                            rows = WebDriverWait(self.driver, 20).until(ec.visibility_of_all_elements_located((By.XPATH, '//tr[@class="GMDataRow"]')))

                            rows_len = len(rows)

                            self.logger.info("Exact rows >>> "+str(rows_exact)+" Rows number >>> "+str(rows_len))

                            if int(rows_exact)*2 != rows_len:

                                self.logger.critical(fa_case + ': Rows number does not match !!!')

                                self.err.update({fa_case: 'Rows number does not match'})

                            row_start = int(rows_len/2)
                            
                            for i in range(row_start, rows_len):

                                row = rows[i]
                                
                                self.logger.debug(row)

                                entries = row.find_elements(By.TAG_NAME,'td')

                                fa_link = entries[3]

                                self.logger.info("FA Link : " + fa_link.text)

                                if fa_case == fa_link.text.strip():

                                    fa_link.click()

                                    sleep(1)
                                    
                                    break
                                
                        else:

                            self.logger.critical(fa_case+" : Page is not rendering !!!")

                            WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()      
                                
                WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//select[@name="TABLE_VIEWS_LIST_1"]//option[@title="QIT_Disposition"]'))).click()

                sleep(1)

                rows_exact = int(WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//strong[@id="totalCount_PSRTABLE_AFFECTEDITEMS"]'))).text)

                self.logger.debug(str(rows_exact))
                
            else:
                
                #exit loop more than 30 cases
                break
            
            
        # get farc_case

        if send_for_rc_rp:

            if open_farc:

                self.farc_fa_case.append(fa_case)

                while True:

                    try:

                        WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()

                        break
                    
                    except:

                        self.logger.warning("Wait for against click intercepted !!!")

                        sleep(1)
                
                sleep(1)

                #add fix bug in case that change priority 

                while True:

                    try:
                
                        WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//span[@id="Actionsspan"]'))).click()

                        break
                    
                    except:

                        self.logger.warning("This case wase changed the priority !!!")

                        if fa_case in WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//h4[@id="searchResultHeader"]'))).text:

                            rows_exact = int(WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//strong[@id="totalCount_QUICKSEARCH_TABLE"]'))).text)

                            self.logger.debug(str(rows_exact))

                            rows = WebDriverWait(self.driver, 20).until(ec.visibility_of_all_elements_located((By.XPATH, '//tr[@class="GMDataRow"]')))

                            rows_len = len(rows)

                            self.logger.info("Exact rows >>> "+str(rows_exact)+" Rows number >>> "+str(rows_len))

                            if int(rows_exact)*2 != rows_len:

                                self.logger.critical(fa_case + ': Rows number does not match !!!')

                                self.err.update({fa_case: 'Rows number does not match'})

                            row_start = int(rows_len/2)
                            
                            for i in range(row_start, rows_len):

                                row = rows[i]
                                
                                self.logger.debug(row)

                                entries = row.find_elements(By.TAG_NAME,'td')

                                fa_link = entries[3]

                                self.logger.info("FA Link : " + fa_link.text)

                                if fa_case == fa_link.text.strip():

                                    fa_link.click()

                                    sleep(1)
                                    
                                    break
                                
                        else:

                            self.logger.critical(fa_case+" : Page is not rendering !!!")

                            WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click() 

                sleep(1)

                WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//*[ text() = "Create OEM / Root Cause" ]'))).click()
                sleep(1)


            #add SRE Symptom
            self.update_sre_symtom(fa_case)

            #move case to PCA
            self.logger.info('Current status of '+fa_case+' >>> '+self.fa_status)

            if self.fa_status != 'Pending Closure Approval':

                self.logger.debug('Move case to >>> PCA...')

                sleep(3)

                self.move_case(fa_case,'PCA')
                
        return True


    def get_farc_case(self,fa_case):

        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(Keys.CONTROL+'a',Keys.BACKSPACE)

        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(fa_case)

        while True:

            try:

                WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()

                break
            
            except:

                self.logger.warning("Wait for against click intercepted !!!")

                sleep(1)

        #add fix bug in case that change priority 
        while True:

            try:
        
                WebDriverWait(self.driver, 20).until(ec.visibility_of_all_elements_located((By.XPATH, '//div[@id="tabsDiv"]//li')))[-3].click()

                break

            except:

                self.logger.warning("This case wase changed the priority !!!")

                if fa_case in WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//h4[@id="searchResultHeader"]'))).text:

                    rows_exact = int(WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//strong[@id="totalCount_QUICKSEARCH_TABLE"]'))).text)

                    self.logger.debug(str(rows_exact))

                    rows = WebDriverWait(self.driver, 20).until(ec.visibility_of_all_elements_located((By.XPATH, '//tr[@class="GMDataRow"]')))

                    rows_len = len(rows)

                    self.logger.info("Exact rows >>> "+str(rows_exact)+" Rows number >>> "+str(rows_len))

                    if int(rows_exact)*2 != rows_len:

                        self.logger.critical(fa_case + ': Rows number does not match !!!')

                        self.err.update({fa_case: 'Rows number does not match'})

                    row_start = int(rows_len/2)
                    

                    for i in range(row_start, rows_len):

                        row = rows[i]
                        
                        self.logger.debug(row)

                        entries = row.find_elements(By.TAG_NAME,'td')

                        fa_link = entries[3]

                        self.logger.info("FA Link : " + fa_link.text)

                        if fa_case == fa_link.text.strip():

                            fa_link.click()

                            sleep(1)
                            
                            break
                else:

                    self.logger.critical(fa_case+" : Page is not rendering !!!")
                    
                    WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(Keys.CONTROL+'a',Keys.BACKSPACE)

                    WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(fa_case)

                    WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()
                    
        sleep(1)
        

        rows_exact = int(WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//strong[@id="totalCount_PR_RELATIONSHIPS"]'))).text)
        
        if int(rows_exact) == 0:
            
            return False

        rows = WebDriverWait(self.driver, 20).until(ec.visibility_of_all_elements_located((By.XPATH, '//tr[@class="GMDataRow"]')))
    
        rows_len = len(rows)

        self.logger.info("Exact rows >>> "+str(rows_exact)+" Rows number >>> "+str(rows_len))

        if int(rows_exact)*2 != rows_len:

            self.logger.critical(fa_case + ': Rows number does not match !!!')

            self.err.update({fa_case: 'Rows number does not match'})

        row_start = int(rows_len/2)
        
        for i in range(row_start, rows_len):

            row = rows[i]

            self.logger.debug(row)

            entries = row.find_elements(By.TAG_NAME,'td')

            farc_case = entries[3]
            
            if 'farc' in farc_case.text.lower():

                self.logger.info("FARC Case >>> "+farc_case.text)

                self.open_farc.update({farc_case.text.strip():fa_case})

        sleep(1)


    def perform_farc(self,farc_case):

        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(Keys.CONTROL+'a',Keys.BACKSPACE)
        
        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(farc_case)

        while True:

            try:

                WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()

                break
            
            except:

                self.logger.warning("Wait for against click intercepted !!!")

                sleep(1)

        #check SN of FARC

        WebDriverWait(self.driver, 20).until(ec.visibility_of_all_elements_located((By.XPATH, '//div[@id="tabsDiv"]//li')))[-5].click()

        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//select[@name="TABLE_VIEWS_LIST_1"]//option[@title="open_FARC"]'))).click()

        sleep(1)

        rows = WebDriverWait(self.driver, 20).until(ec.visibility_of_all_elements_located((By.XPATH, '//tr[@class="GMDataRow"]')))

        ## get html page source

        # self.htmlSource = self.driver.page_source

        # with open("htmlSource.txt",'w',encoding='utf-8') as f:

        #     f.write(self.htmlSource)
    

        row = rows[1]

        self.logger.debug(row)

        entries = row.find_elements(By.TAG_NAME,'td')

        farc_sn = entries[1].text.strip()

        self.logger.info("FARC SN>>> "+farc_sn)

        #perform FARC
        while True:

            try:

                WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()

                break
            
            except:

                self.logger.warning("Wait for against click intercepted !!!")

                sleep(1)

        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//span[@id="MSG_Editspan"]'))).click()
        sleep(1) 
        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//span[@id="show_floater_R1_4043_0_displayspan"]'))).click()
        sleep(1) 
        WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//input[@id="floater_search_text_R1_4043_0_display"]'))).send_keys(self.fa_dict[self.open_farc[farc_case]][farc_sn][2])
        sleep(1) 
        WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//span[@id="searchspan"]'))).click()
        sleep(1)
        elm_select = WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//td[@class=" yui-dt-string yui-dt-first"]')))
        ActionChains(self.driver).double_click(on_element = elm_select).perform()
        sleep(1)
        WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//textarea[@name="R1_4038_0"]'))).send_keys(self.fa_dict[self.open_farc[farc_case]][farc_sn][1])
        sleep(1)
        WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="close_floater_R1_4043_0_display"]'))).click() 
        sleep(1)
        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//span[@id="MSG_Savespan"]'))).click()
        sleep(1)

        self.move_case(farc_case,'RMA')
        
        #get farc result parameter
        
        fa_case = self.open_farc[f'{farc_case}']
        
        self.farc_res.update({f'{farc_case}':[fa_case,farc_sn,self.fa_dict[fa_case][farc_sn][3]]})


    def move_case(self,case,target):

        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(Keys.CONTROL+'a',Keys.BACKSPACE)

        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(case)

        self.logger.info('FA Case >>> '+case)

        while True:

            try:

                WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()

                break
            
            except:

                self.logger.warning("Wait for against click intercepted !!!")

                sleep(1)


        #add fix bug in case that change priority 

        while True:

            try:
                
                fa_case_status = WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//h2[@style="color:Blue;"]'))).text
        
                break

            except:

                self.logger.warning("This case wase changed the priority !!!")

                if case in WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//h4[@id="searchResultHeader"]'))).text:

                    rows_exact = int(WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//strong[@id="totalCount_QUICKSEARCH_TABLE"]'))).text)

                    self.logger.debug(str(rows_exact))

                    rows = WebDriverWait(self.driver, 20).until(ec.visibility_of_all_elements_located((By.XPATH, '//tr[@class="GMDataRow"]')))

                    rows_len = len(rows)

                    self.logger.info("Exact rows >>> "+str(rows_exact)+" Rows number >>> "+str(rows_len))

                    if int(rows_exact)*2 != rows_len:

                        self.logger.critical(case + ': Rows number does not match !!!')

                        self.err.update({case: 'Rows number does not match'})

                    row_start = int(rows_len/2)
                    

                    for i in range(row_start, rows_len):

                        row = rows[i]
                        
                        self.logger.debug(row)

                        entries = row.find_elements(By.TAG_NAME,'td')

                        fa_link = entries[3]

                        self.logger.info("FA Link : " + fa_link.text)

                        if case == fa_link.text.strip():

                            fa_link.click()

                            sleep(1)
                            
                            break
                else:

                    self.logger.critical(case+" : Page is not rendering !!!")
                    
                    WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(Keys.CONTROL+'a',Keys.BACKSPACE)

                    WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(case)

                    WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()
                    

        self.logger.info("Move case from >>>"+fa_case_status+" to >>> "+target)

        not_reset_audit = True

        while(not_reset_audit):

            try :   

                WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//em[@id="MSG_NextStatus_em"]'))).click()

                if target == 'PCA':

                    WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//*[ text() = "Pending Closure Approval" ]'))).click()
                    
                    WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//a[@id="ewfinish"]'))).click()


                elif target == 'RMA':

                    WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//span[@id="MSG_NextStatusspan"]'))).click()
                    
                    WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//a[@id="ewfinish"]'))).click()


                elif target == 'FI':
                    
                    WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//*[ text() = "Fault Isolation" ]'))).click()
                    
                    
                not_reset_audit = False
                
            except:

                reset_handle = False

                handles = self.driver.window_handles

                self.logger.debug("No. Of window handles1: "+str(len(handles))+", "+str(handles))

                for handle in handles:

                    self.driver.switch_to.window(handle)

                    window_title = self.driver.title

                    if window_title == 'Application Error':

                        self.logger.error("Application Error Window: "+window_title+" , " +handle)

                        reset_handle = True

                        self.driver.close()

                if reset_handle:
                    
                    self.driver.switch_to.window(self.main_page)

                    while True:

                        try:

                            WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()

                            break
                        
                        except:

                            self.logger.warning("Wait for against click intercepted !!!")

                            sleep(1)


        #window handle
        found_change_status_window = False
        count_open_change_status_window = 0
        while(not found_change_status_window):

            reset_handle = False
            sleep(1)#9-Dec-2021  add delay
            handles = self.driver.window_handles
            size = len(handles)
            self.logger.debug("No. Of window handles2: "+str(size)+' >>>  '+str(handles))

            for handle in handles:
                
                self.driver.switch_to.window(handle)
                window_title = self.driver.title
                if window_title == 'Change Status':
                    self.logger.debug("Change Status Window: "+window_title+' >>> '+str(handles))
                    found_change_status_window = True
                    break
                elif window_title == 'Application Error':
                    self.logger.error("Application Error Window: "+window_title+' >>> '+str(handles))

                    sleep(1)#9-Dec-2021  add delay
                    reset_handle = True
                    sleep(1)#9-Dec-2021  add delay
                    self.driver.close()

            if reset_handle:
                
                self.driver.switch_to.window(self.main_page)
                
                WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//span[@id="MSG_NextStatusspan"]'))).click()  #9-Dec-2021  visible to clickable
                
                WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="ewfinish"]'))).click()    #9-Dec-2021
                
            count_open_change_status_window+=3
            
            if count_open_change_status_window > 10:
            
                self.logger.critical("Can't open change_status Window: "+str(count_open_change_status_window)+" second")
                self.can_not_update_state[case] = "Can't Open Change Status Window"
                break

        

        if target == 'PCA':

            WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//a[@class="delete_button"]'))).click()  

            sleep(1)

            WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@id="search_query_approvers_display"]'))).send_keys('wlongsid'+Keys.ENTER)
            
        elif target == 'FI':

            WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//a[@class="delete_button"]'))).click()  

            sleep(1)

            WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//input[@id="search_query_notify_display"]'))).send_keys('wlongsid'+Keys.ENTER)        

        sleep(5)              

        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//a[@id="save"]'))).click()  


        count_close_change_status_window = 0
        while (len(self.driver.window_handles) > 1):
            sleep(1)
            count_close_change_status_window+=1
            self.logger.warning("Wait for Close Change Status Window: "+ str(count_close_change_status_window)+" second")
            if count_close_change_status_window > 10:
                self.logger.critical("Can't Close Change Status Window: "+str(count_close_change_status_window))
                self.can_not_update_state[case] = "Can't Close Change Status Window"
                sleep(1)  #9-Dec-2021  add delay
                self.driver.close()
                sleep(1)  #9-Dec-2021  add delay
                break       
        sleep(1)
        
        self.driver.switch_to.window(self.main_page)      


    def update_sre_symtom(self,fa_case):

        while True:

            try:

                WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()

                break
            
            except:

                self.logger.warning("Wait for against click intercepted !!!")

                sleep(1)
        
        sleep(1)

        #add fix bug in case that change priority 

        while True:

            try:
        
                WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//span[@id="Actionsspan"]'))).click()

                break

            except:

                self.logger.warning("This case wase changed the priority !!!")

                if fa_case in WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//h4[@id="searchResultHeader"]'))).text:

                    rows_exact = int(WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, '//strong[@id="totalCount_QUICKSEARCH_TABLE"]'))).text)

                    self.logger.debug(str(rows_exact))

                    rows = WebDriverWait(self.driver, 20).until(ec.visibility_of_all_elements_located((By.XPATH, '//tr[@class="GMDataRow"]')))

                    rows_len = len(rows)

                    self.logger.info("Exact rows >>> "+str(rows_exact)+" Rows number >>> "+str(rows_len))

                    if int(rows_exact)*2 != rows_len:

                        self.logger.critical(fa_case + ': Rows number does not match !!!')

                        self.err.update({fa_case: 'Rows number does not match'})

                    row_start = int(rows_len/2)
                    

                    for i in range(row_start, rows_len):

                        row = rows[i]
                        
                        self.logger.debug(row)

                        entries = row.find_elements(By.TAG_NAME,'td')

                        fa_link = entries[3]

                        self.logger.info("FA Link : " + fa_link.text)

                        if fa_case == fa_link.text.strip():

                            fa_link.click()

                            sleep(1)
                            
                            break

                else:

                    self.logger.critical(fa_case+" : Page is not rendering !!!")

                    WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()

        sleep(1)

        WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, '//*[ text() = "Update Final SRE Symptom Code Category and Sub Category" ]'))).click()
        sleep(1)


        #swithch window
        found_change_status_window = False

        while not found_change_status_window:
            sleep(1)
            handles = self.driver.window_handles
            size = len(handles)
            self.logger.debug("No. Of window handles2: "+str(size)+' >>>  '+str(handles))

            for handle in handles:
                self.driver.switch_to.window(handle)
                window_title = self.driver.title
                if 'sre' in window_title.lower():
                    self.logger.debug("Window Title: "+window_title+' >>> '+str(handles))
                    self.driver.maximize_window()
                    found_change_status_window = True
                    break

        
        
        while True:

            try:

                alert = WebDriverWait(self.driver, 5).until(ec.element_to_be_clickable((By.XPATH, '//span[@id="hopSection"]'))).text

                self.logger.warning("SRE Alert Text: "+alert)

                if 'sre' in alert.lower():

                    self.logger.debug("Not found SRE symptom")

                    sleep(1)

                    self.driver.close()

                    self.driver.switch_to.window(self.main_page)    

                    return


            except:

                try:

                    btn_txt = WebDriverWait(self.driver, 20).until(ec.element_to_be_clickable((By.XPATH, f'//button[@id="issueSubmit"]'))).text

                    self.logger.warning("Button Text: "+btn_txt)

                    if 'update' in btn_txt.lower():

                        self.logger.debug("Not found Update Button")

                        break

                except:

                    self.logger.warning("Wait for SRE page render !!!")


        row = 0

        while True:

            try:

                WebDriverWait(self.driver, 5).until(ec.element_to_be_clickable((By.XPATH, f'//tr[@id="row{str(row)}"]//li[@aria-level="2"]//i[@role="presentation"]'))).click()

                sleep(1)

                try:

                    WebDriverWait(self.driver, 3).until(ec.element_to_be_clickable((By.XPATH, f'//tr[@id="row{str(row)}"]//td[@id="issueClass"]//*[ text() = "Pluggable failed" ]'))).click()

                    sleep(1)

                except:
                    
                    
                    try:

                        WebDriverWait(self.driver, 3).until(ec.element_to_be_clickable((By.XPATH, f'//tr[@id="row{str(row)}"]//td[@id="issueClass"]//*[ text() = "Pluggable fails" ]'))).click()

                        sleep(1)
                        
                    except:
                        
                        # handle with alert
                        try:
                            WebDriverWait(self.driver, 20).until(ec.alert_is_present())

                            alert = self.driver.switch_to.alert
                            alert.accept()
                            self.logger.debug("alert accepted")

                        except:
                            self.logger.debug("no alert")
                            
                            try:
                            
                                self.driver.close()
                                
                            except:
                                
                                pass

                        self.driver.switch_to.window(self.main_page)
                    
                        return False
                    
                        

                sn = WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located((By.XPATH, f'//tr[@id="row{str(row)}"]//td[@id="siteSN"]'))).text

                self.logger.debug(sn+" : Pluggable failed")

                row+=1


            except:

                try:

                    WebDriverWait(self.driver, 5).until(ec.element_to_be_clickable((By.XPATH, f'//button[@id="issueSubmit"]'))).click()

                    break
                
                except:
                    
                    break
                    
                    
        sleep(5)
        
        # handle with alert
        try:
            WebDriverWait(self.driver, 20).until(ec.alert_is_present())

            alert = self.driver.switch_to.alert
            alert.accept()
            self.logger.debug("alert accepted")

        except:
            
            self.logger.debug("no alert")
            
            try:

                self.driver.close()
            
            except:
                
                pass


            
        self.driver.switch_to.window(self.main_page)
        
        return True   

    
    def press_down(self,time =1):

        for i in range(time):

            ActionChains(self.driver).send_keys(Keys.DOWN).perform()


    def press_enter(self,time =1):

        for i in range(time):

            ActionChains(self.driver).send_keys(Keys.ENTER).perform()



    def main(self):

        self.login()

        self.extract_data_excel()

        for fa_case in self.fa_dict:

            self.dispose_affected_items(fa_case)

        # for fa_case_open_farc in self.fa_dict:

        #     self.get_farc_case(fa_case_open_farc)

        for fa_case_open_farc in self.farc_fa_case:

            self.get_farc_case(fa_case_open_farc)


        for farc_case in self.open_farc:

            self.perform_farc(farc_case)


        if len(self.farc_res) >0:
            
            for farc in self.farc_res:
                
                self.logger.info(farc+'  >  '+self.farc_res[farc][0]+'  >  '+self.farc_res[farc][1]+'  >  '+self.farc_res[farc][2])
            
        self.logger.critical(self.err)
        
    

if __name__ == "__main__":

    try:

        run = QIT_DISPOSITION()

        run.main()

    except:

        tb = traceback.format_exc()

        run.logger.critical(tb)

    while input('Please enter \'e\' for exit!') != 'e':

        pass

    run.driver.quit()

    sys.exit()








