import logging
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from time import sleep
import pandas as pd
import configparser
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import os
import re
import sys
import pyautogui
import datetime



class QIT_DISPOSITION:

    def __init__(self):

        # create logger
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)

        # create console handler
        ch = logging.StreamHandler()

        #create file handler 
        date = str(datetime.datetime.now().strftime('%d-%b-%Y %H_%M_%S %p'))
        fh = logging.FileHandler('debug\\{}.log'.format(date),encoding='utf-8')

        # create formatter
        formatter = logging.Formatter('%(asctime)s - %(funcName)s - %(levelname)s - %(message)s',datefmt='%d/%b/%Y %I:%M:%S %p')

        # add formatter to ch
        ch.setFormatter(formatter)

        #add formatter to fh
        fh.setFormatter(formatter)

        # add ch to logger
        self.logger.addHandler(ch)

        #add fh to logger
        self.logger.addHandler(fh)


        #config.init file
        self.my_config_parser = configparser.ConfigParser()

        self.my_config_parser.read('config\\config.ini')

        self.config = { 

        'email': self.my_config_parser.get('QIT_DISPOSITION_CONFIG','email'),
        'password': self.my_config_parser.get('QIT_DISPOSITION_CONFIG','password'),

        }


        pyautogui.PAUSE = 0.3


        self.err = {}

        self.open_farc = {}

        self.farc_fa_case = []

        self.can_not_update_state = {}




    def login(self):

        self.driver=webdriver.Chrome()

        self.driver.get("https://www-plmprd.cisco.com/Agile/")


        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//input[@id="userInput"]'))).send_keys(self.config["email"])

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="login-button"]'))).click()

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//input[@id="passwordInput"]'))).send_keys(self.config["password"])

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//input[@id="login-button"]'))).click()

        count_render_2fa = 0

        while (self.driver.title != "Two-Factor Authentication"):

            sleep(1)

            count_render_2fa+=1

            self.logger.info("Wait for two-factor authentication render:"+str(count_render_2fa))


        push_login_btn = WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//button[@type="submit"]')))

        push_login_btn_text = push_login_btn.text

        if push_login_btn_text == "Send Me a Push":

            clickable = False

            while(not clickable):

                try:

                    WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//button[@type="submit"]'))).click()

                    clickable = True

                except:

                    self.logger.warning("Can't Clickable'")
                
        else:
            self.logger.info("Not found \"Send Me a Push\" button")


        two_fa_url=self.driver.current_url

        count_duo_pass = 0

        while(two_fa_url==self.driver.current_url):

            sleep(1)

            count_duo_pass+=1

            self.logger.info("Wait for count_duo_pass:"+str(count_duo_pass))

            if count_duo_pass == 30:

                self.logger.warning("!!! LOGIN TIMEOUT !!!")

                self.driver.quit()

                sys.exit()

        self.logger.info("Login to QIS is success!!!")

        sleep(1)

        self.driver.get("https://www-plmprd.cisco.com/Agile/")

        self.main_page = self.driver.current_window_handle

        self.logger.debug("Main Page:"+self.main_page)

        handles = self.driver.window_handles

        for handle in handles:

            sleep(1)

            self.driver.switch_to.window(handle)

            if self.main_page != self.driver.current_window_handle:

                self.driver.close()

        self.driver.switch_to.window(self.main_page)

        self.driver.maximize_window()

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//div[@title="Collapse Left Navigation"]'))).click()

        return True


    def extract_data_excel(self):                                                                                                                                            

        self.fa_dict = {}

        self.df = pd.read_excel("src\\qit_disposition_template.xlsx")
        
        self.logger.info(self.df)

        index = self.df.index

        number_of_rows = len(index)
        
        self.logger.info('Number of rows >>> '+str(number_of_rows))

        for i in range(number_of_rows):

            if self.df['FA#'][i] in self.fa_dict:

                self.fa_dict[self.df['FA#'][i]].update({self.df['Site Received Serial#'][i]:[self.df['QIT Disposition'][i], self.df['Problem Description'][i],self.df['Case Owner'][i]]})
            
            else:

                self.fa_dict[self.df['FA#'][i]] = {self.df['Site Received Serial#'][i]: [self.df['QIT Disposition'][i], self.df['Problem Description'][i],self.df['Case Owner'][i]]}

        for i,j in self.fa_dict.items():

            self.logger.debug(i + str(j))

        self.logger.info('FA Case number >>> '+str(self.fa_dict.__len__()) + ' Cases')



    def dispose_affected_items(self,fa_case):

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(Keys.CONTROL+'a',Keys.BACKSPACE)

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(fa_case)

        while True:

            try:

                WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()

                break
            
            except:

                self.logger.warning("Wait for against click intercepted !!!")


        WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.XPATH, '//span[@id="MSG_Editspan"]'))).click()

        WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.XPATH, '//select[@id="R1_2023_7"]//*[ text() = "Fault Isolated" ]'))).click()


        sleep(1)
        WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.XPATH, '//span[@id="MSG_Savespan"]'))).click()




    def get_farc_case(self,fa_case):

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(Keys.CONTROL+'a',Keys.BACKSPACE)

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(fa_case)

        while True:

            try:

                WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()

                break
            
            except:

                self.logger.warning("Wait for against click intercepted !!!")

        WebDriverWait(self.driver, 10).until(ec.visibility_of_all_elements_located((By.XPATH, '//div[@id="tabsDiv"]//li')))[-3].click()
        sleep(1)

        rows_exact = int(WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//strong[@id="totalCount_PR_RELATIONSHIPS"]'))).text)

        rows = WebDriverWait(self.driver, 10).until(ec.visibility_of_all_elements_located((By.XPATH, '//tr[@class="GMDataRow"]')))
    
        rows_len = len(rows)

        self.logger.info("Exact rows >>> "+str(rows_exact)+" Rows number >>> "+str(rows_len))

        if int(rows_exact)*2 != rows_len:

            self.logger.critical(fa_case + ': Rows number does not match !!!')

            self.err.update({fa_case: 'Rows number does not match'})

        row_start = int(rows_len/2)
        

        for i in range(row_start, rows_len):

            row = rows[i]

            self.logger.debug(row)

            entries = row.find_elements(By.TAG_NAME,'td')

            farc_case = entries[3]

            self.logger.info("FARC Case >>> "+farc_case.text)

            self.open_farc.update({farc_case.text.strip():fa_case})

        sleep(1)



    def perform_farc(self,farc_case):

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(Keys.CONTROL+'a',Keys.BACKSPACE)

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(farc_case)

        while True:

            try:

                WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()

                break
            
            except:

                self.logger.warning("Wait for against click intercepted !!!")

        #check SN of FARC

        WebDriverWait(self.driver, 10).until(ec.visibility_of_all_elements_located((By.XPATH, '//div[@id="tabsDiv"]//li')))[-5].click()

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//select[@name="TABLE_VIEWS_LIST_1"]//option[@title="open_FARC"]'))).click()

        sleep(1)

        rows = WebDriverWait(self.driver, 10).until(ec.visibility_of_all_elements_located((By.XPATH, '//tr[@class="GMDataRow"]')))

        ## get html page source

        # self.htmlSource = self.driver.page_source

        # with open("htmlSource.txt",'w',encoding='utf-8') as f:

        #     f.write(self.htmlSource)
    

        row = rows[1]

        self.logger.debug(row)

        entries = row.find_elements(By.TAG_NAME,'td')

        farc_sn = entries[1].text.strip()

        self.logger.info("FARC SN>>> "+farc_sn)

        #perform FARC
        while True:

            try:

                WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()

                break
            
            except:

                self.logger.warning("Wait for against click intercepted !!!")

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//span[@id="MSG_Editspan"]'))).click()
        sleep(1) 
        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//span[@id="show_floater_R1_4043_0_displayspan"]'))).click()
        sleep(1) 
        WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.XPATH, '//input[@id="floater_search_text_R1_4043_0_display"]'))).send_keys(self.fa_dict[self.open_farc[farc_case]][farc_sn][2])
        sleep(1) 
        WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.XPATH, '//span[@id="searchspan"]'))).click()
        sleep(1)
        elm_select = WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.XPATH, '//td[@class=" yui-dt-string yui-dt-first"]')))
        ActionChains(self.driver).double_click(on_element = elm_select).perform()
        sleep(1)
        WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.XPATH, '//textarea[@name="R1_4038_0"]'))).send_keys(self.fa_dict[self.open_farc[farc_case]][farc_sn][1])
        sleep(1)
        WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="close_floater_R1_4043_0_display"]'))).click() 
        sleep(1)
        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//span[@id="MSG_Savespan"]'))).click()
        sleep(1)

        self.move_case(farc_case,'RMA')



    def move_case(self,case,target):

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(Keys.CONTROL+'a',Keys.BACKSPACE)

        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//input[@name="QUICKSEARCH_STRING"]'))).send_keys(case)

        self.logger.info('FA Case >>> '+case)

        while True:

            try:

                WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()

                break
            
            except:

                self.logger.warning("Wait for against click intercepted !!!")

        not_reset_audit = True

        while(not_reset_audit):

            try :   

                WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//em[@id="MSG_NextStatus_em"]'))).click()

                if target == 'PCA':

                    WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.XPATH, '//*[ text() = "Pending Closure Approval" ]'))).click()


                elif target == 'RMA':

                    WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.XPATH, '//*[ text() = "RMA" ]'))).click()


                WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//a[@id="ewfinish"]'))).click()

                not_reset_audit = False
                
            except:

                reset_handle = False

                handles = self.driver.window_handles

                self.logger.debug("No. Of window handles1: "+str(len(handles))+", "+str(handles))

                for handle in handles:

                    self.driver.switch_to.window(handle)

                    window_title = self.driver.title

                    if window_title == 'Application Error':

                        self.logger.error("Application Error Window: "+window_title+" , " +handle)

                        reset_handle = True

                        self.driver.close()

                if reset_handle:
                    
                    self.driver.switch_to.window(self.main_page)

                    while True:

                        try:

                            WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="top_simpleSearch"]'))).click()

                            break
                        
                        except:

                            self.logger.warning("Wait for against click intercepted !!!")


        #window handle
        found_change_status_window = False
        count_open_change_status_window = 0
        while(not found_change_status_window):

            reset_handle = False
            sleep(1)#9-Dec-2021  add delay
            handles = self.driver.window_handles
            size = len(handles)
            self.logger.debug("No. Of window handles2: "+str(size)+' >>>  '+str(handles))

            for handle in handles:
                
                self.driver.switch_to.window(handle)
                window_title = self.driver.title
                if window_title == 'Change Status':
                    self.logger.debug("Change Status Window: "+window_title+' >>> '+str(handles))
                    found_change_status_window = True
                    break
                elif window_title == 'Application Error':
                    self.logger.error("Application Error Window: "+window_title+' >>> '+str(handles))

                    sleep(1)#9-Dec-2021  add delay
                    reset_handle = True
                    sleep(1)#9-Dec-2021  add delay
                    self.driver.close()

            if reset_handle:
                
                self.driver.switch_to.window(self.main_page)
                
                WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.XPATH, '//span[@id="MSG_NextStatusspan"]'))).click()  #9-Dec-2021  visible to clickable
                
                WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.XPATH, '//a[@id="ewfinish"]'))).click()    #9-Dec-2021
                
            count_open_change_status_window+=3
            
            if count_open_change_status_window > 10:
            
                self.logger.critical("Can't open change_status Window: "+str(count_open_change_status_window)+" second")
                self.can_not_update_state[farc_case] = "Can't Open Change Status Window"
                break


        if count_open_change_status_window < 10:

            WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, '//a[@id="save"]'))).click()                

            count_close_change_status_window = 0
            while (len(self.driver.window_handles) > 1):
                sleep(1)
                count_close_change_status_window+=1
                self.logger.warning("Wait for Close Change Status Window: "+ str(count_close_change_status_window)+" second")
                if count_close_change_status_window > 10:
                    self.logger.critical("Can't Close Change Status Window: "+str(count_close_change_status_window))
                    self.can_not_update_state[farc_case] = "Can't Close Change Status Window"
                    sleep(1)  #9-Dec-2021  add delay
                    self.driver.close()
                    sleep(1)  #9-Dec-2021  add delay
                    break       
        sleep(1)
        
        self.driver.switch_to.window(self.main_page)      




    def main(self):

        self.login()

        self.extract_data_excel()

        for fa_case in self.fa_dict:

            self.dispose_affected_items(fa_case)

        # for fa_case_open_farc in self.farc_fa_case:

        #     self.get_farc_case(fa_case_open_farc)

        # for farc_case in self.open_farc:

        #     self.perform_farc(farc_case)

        # self.logger.critical(self.err)

        


if __name__ == "__main__":

    run = QIT_DISPOSITION()

    run.main()

    while input('Please enter \'e\' for exit!') != 'e':

        pass

    run.driver.quit()

    sys.exit()









